********** Notes on This Tox21 Data Challenge Repository (and some background on Tox21 itself) **********

The Tox21 challenge was a supervised learning predictive modeling challenge conducted by a consortium of US governmental agencies (EPA, NTP, NIEHS, NCATS, and FDA).  The predictive modeling portion of the challenge ended in 2015, but this challenge still serves as a valuable benchmark.  The goal of the challenge was to best predict 12 toxicities ("tasks") for thousands of molecules.  Each of the 12 tasks is binary- a molecule is associated with a toxicity (1) or it isn't (0).  Note that there are many missing labels- most molecules are missing some class labels for at least some of the tasks.  More than 50 teams participated in this challenge, but most teams did not generate predictions for all 12 tasks.  

The main figure of mertit for the challenge was AUROC (Area Under ROC).  AUROC was averaged on a test set for each of the 12 tasks, and the average AUROC was used to rank challenge participants.  AUROC is a commonly used metric, but note that many of the 12 tasks are severely imbalanced towards Class 0 (non-toxic).  Some tasks are as imbalanced as 20 Class 0 molecules for every 1 Class 1 molecule.  As a result, teams with heavy biases towards Class 0 predictions tended to produce higher AUROCs and dominated the rankings of the competition, despite often having poor predictive skill for Class 1 (toxic molecules).  On the final test set, the winning team (Bioinformatics Group of Johannes Kepler University) had an average sensitivity (true positive rate) of only ~30%.  I would have preferred a data metric that more accurately compensated for severely imbalanced classes (something like Balanced Accuracy), but that is just editorial commentary from me.  


Notes on the 12 tasks that are modeled
Descriptions of each of these 12 tasks can be found at: https://tripod.nih.gov/tox21/challenge/about.jsp
There are 7 "Nuclear Response" (NR) toxicities:
NR.AhR
NR.AR
NR.AR.LBD
NR.Aromatase
NR.ER
NR.ER.LBD
NR.PPAR.gamma

There are 5 "Stress Response" (SR) toxicities:
SR.ARE
SR.ATAD5 
SR.HSE 
SR.MMP
SR.p53


Notes on the data itself:
The "raw" data provided by the challenge organizers contained many duplicate molecules and invalid molecules in the provided .sdf files.  The cleaned-up .mol files and associated class label files are in this folder.  We split the raw .sdf files into mol files to facilitate loading data in batches.  

TrainSetMols: this is a folder of 10808 sanitized mol files for the training set that was used by Tox21 challenge participants.

TestSetMols: this is a folder containing 646 sanitized mol files for the "final" test set that was used by Tox21 participants.  NOTE: that there was a "leaderboard" test set used during the competition.  The leaderboard test set was significantly easier to generate skilled predictions on than the final test set, and nearly every team performed better on the leaderboard test set than on the final test set.  Most papers that I've read on this challenge report the more encouraging numbers obtained from the leaderboard test set.  

tox21_labels_train_dedup.csv: a .csv file containing class labels (which molecules have which labels for each task) for all molecules in the training set.  Duplicated / invalid molecules have been excluded.

tox21_labels_test_dedup.csv: a .csv file containing class labels (which molecules have which labels for each task) for all molecules in the final test set.  Duplicated / invalid molecules have been excluded.  


Bob D'Agostino
bdagostino@spektronsystems.com
